<?php
  
  class Api_model extends CI_Model {
      
      function exist_user_name($user_name) {
          
          $this->db->where('username', $user_name);
          $query = $this->db->get('ci_users');
          if ($query->num_rows() > 0) {
              return true;
          } else {
              return false;
          }
      }                 
      
      function exist_user_email($email) {
          
          $query = $this->db->get_where('ci_users', array('email' => $email, 'role' => 1));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function exist_user_email_id($user_id, $email) {
          
          $this->db->where('id !=', $user_id);
          $this->db->where('email', $email);
          $this->db->where('role', 1);
          
          $query = $this->db->get('ci_users');          
          
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function add_user($data) {
          
          $this->db->insert('ci_users', $data);
          return $this->db->insert_id();
      }
      
      public function login($data){
            $query = $this->db->get_where('ci_users', array('email' => $data['email'], 'role' => 1, 'is_active' => 2));
            if ($query->num_rows() == 0){
                return false;
            }
            else{
                //Compare the password attempt with the password we have stored.
                $result = $query->row_array();
                $validPassword = password_verify($data['password'], $result['password']);
                if($validPassword){
                    return $result = $query->row_array();
                }
                
            }
      }
      
      public function facebookSignin($facebook_id) {
          
          $query = $this->db->get_where('ci_users', array('facebook_id' => $facebook_id));
            if ($query->num_rows() == 0){
                return false;
            }
            else{
                //Compare the password attempt with the password we have stored.                  
                return $result = $query->row_array();
            }
      }
      
      function resetPassword($data) {
          $this->db->where('email', $data['email']);
          $this->db->where('role', 1);
          $this->db->set('password', $data['password']);
          $this->db->update('ci_users');
          return true;
      }
      
      function getServiceList($service_type) {
          
          $this->db->where('service_type', $service_type);
          $this->db->order_by('id', 'ASC');
          $query = $this->db->get('ci_services');
          
          if ($query->num_rows() > 0) {              
              return $query->result_array();
          } else {
              return false;
          } 
      }
      
      function makeOrder($data) {
          
          $this->db->insert('ci_orders', $data);
          return $this->db->insert_id();         
          
      }
      
      function getMyOrders($user_id) {
          
          $this->db->select('ci_orders.*, ci_users.mobile_no')
                     ->from('ci_orders')
                     ->where('ci_orders.user_id', $user_id)
                     ->join('ci_users', 'ci_users.id = ci_orders.user_id')
                     ->order_by('ci_orders.created_at', 'DESC');
          $query = $this->db->get();
          
          //$query = $this->db->get_where('ci_orders', array('user_id' => $user_id));
          
          if ($query->num_rows() > 0) {
              return $query->result_array();
          } else {
              return false;
          }
          
          
      }
      
      
      // manager + driver 
      
      function addServiceItem($data) {
          
          $this->db->insert('ci_services', $data);
          return true;
      }
      
      public function stuffLogin($data){
            
          $this->db->where('email', $data['email']);
          $this->db->where('role !=', 1);
          $query = $this->db->get('ci_users');
            
          if ($query->num_rows() == 0){
              return false;
          }
          else{
              //Compare the password attempt with the password we have stored.

              $result = $query->row_array();
              $validPassword = password_verify($data['password'], $result['password']);
              if($validPassword){
                return $result = $query->row_array();
              }
                
          }
      }
      
      function exist_stuff_user_email($email) {
          
          $this->db->where('email', $email);
          $this->db->where('role !=', 1);
          
          $query = $this->db->get('ci_users');
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function stuffResetPassword($data) {
          $this->db->where('email', $data['email']);
          $this->db->where('role !=', 1);
          $this->db->set('password', $data['password']);
          $this->db->update('ci_users');
          return true;
      }
      
      function update_driver($user_id, $data) {
          
          $this->db->where('id', $user_id);
          $this->db->update('ci_users', $data);
      }
      
      function getDriverCustomerList() {
          
          $this->db->where('is_admin !=', 1);
          $this->db->where('role !=', 3);
          $this->db->order_by('id', 'ASC');
          $query = $this->db->get('ci_users');
          
          if ($query->num_rows() == 0) {
              return false;
          } else {
              return $query->result_array();
          }          
      }
      
      function getCustomerList() {
          
          $this->db->where('is_admin !=', 1);
          $this->db->where('role', 1);
          $this->db->order_by('id', 'ASC');
          $query = $this->db->get('ci_users');
          
          if ($query->num_rows() == 0) {
              return false;
          } else {
              return $query->result_array();
          }          
      }
      
      function getOrderList() {
          
          //$query = $this->db->get('ci_orders');
          
          $this->db->select('ci_orders.*, ci_users.mobile_no')
                     ->from('ci_orders')
                     ->join('ci_users', 'ci_users.id = ci_orders.user_id')
                     ->order_by('ci_orders.created_at', 'DESC');
          $query = $this->db->get();
          
          if ($query->num_rows() > 0) {
              return $query->result_array();
          } else {
              return false;
          }
      }
      
      function sendOffCode($user_id, $data) {
          
          $query = $this->db->get_where('ci_offcode', array('user_id' => $user_id));
          
          if ($query->num_rows() > 0 ) {
              
              $this->db->where('user_id', $user_id);
              $this->db->update('ci_offcode', $data);
              return $query->row()->id;             
              
          } else {
              
              $this->db->insert('ci_offcode', $data);
              return $this->db->insert_id();
              
          }
      }
      
      function getMyOffCode($user_id) {
          
          return $this->db->get_where('ci_offcode', array('user_id' => $user_id))->row_array();         
          
      }    
      
      function updatePhoto($user_id, $data) {
          
          $this->db->where('id', $user_id);
          $this->db->update('ci_users', $data);
          return true;
      }
      
      function updateServiceItem($service_id, $data) {
          
          $this->db->where('id', $service_id);
          $this->db->update('ci_services', $data);
          return true; 
          
      }
      
      function updateServicePhoto($service_id, $data) {
          
          $this->db->where('id', $service_id);
          $this->db->update('ci_services', $data);
          return true;
      }
      
      function removeDriver($user_id) {
          
          $this->db->where('id', $user_id);
          $this->db->where('role !=', 3);
          $this->db->delete('ci_users');
          return true;
      }
      
      function verifyPhoneNumber($user_id, $code) {
          
          $this->db->where('id', $user_id);
          $this->db->where('verification_code', $code);
          
          if ($this->db->get('ci_users')->num_rows() > 0) {
              $this->db->where('id', $user_id);
              $this->db->set('is_active', 2);
              $this->db->update('ci_users');
              return true;
          } else {
              return false;
          }
      }
      
      function validCustomer($email, $phone_number) {
          
          $query = $this->db->get_where('ci_users', array('email' => $email, 
                                                          'mobile_no' => $phone_number,
                                                          'is_active' => 2,
                                                          'role' => 1));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function updateCode($email, $phone_number, $code) {
          
          $this->db->where('email', $email);
          $this->db->where('mobile_no', $phone_number);
          $this->db->set('verification_code', $code);
          $this->db->update('ci_users');
          
      }
      
      function validVerifyCode($email, $verification_code) {
          
          $query = $this->db->get_where('ci_users', array('email' => $email,
                                                  'verification_code' => $verification_code,        
                                                  'is_active' => 2,
                                                  'role' => 1));
          if ($query->num_rows() == 0) {
              return false;              
          } else {
              return true;
          }                                        
                                                  
      }
      
      function update_user($user_id, $data) {
          
          $this->db->where('id', $user_id);
          $this->db->update('ci_users', $data);
      }
      
      function setPrivacyPolicy($privacy_policy) {
          
          if ($this->db->get('ci_aboutus')->num_rows() > 0) {
              
              $this->db->set('privacy_policy', $privacy_policy);
              $this->db->set('updated_at', date('Y-m-d h:m:s'));
              $this->db->update('ci_aboutus');
          } else {
              $data = array('privacy_policy' => $privacy_policy,
                            'created_at' => date('Y-m-d h:m:s'));
              $this->db->insert('ci_aboutus', $data);
              $this->db->insert_id();
          }
      }
      
      function setDistinguishUs($distinguish_us) {
          
          if ($this->db->get('ci_aboutus')->num_rows() > 0) {
              
              $this->db->set('distinguish_us', $distinguish_us);
              $this->db->set('updated_at', date('Y-m-d h:m:s'));
              $this->db->update('ci_aboutus');
          } else {
              $data = array('distinguish_us' => $distinguish_us,
                            'created_at' => date('Y-m-d h:m:s'));
              $this->db->insert('ci_aboutus', $data);
              $this->db->insert_id();
          }         
          
      }
      
      function setTermsConditions($terms_conditions) {
          
          if ($this->db->get('ci_aboutus')->num_rows() > 0) {
              
              $this->db->set('terms_conditions', $terms_conditions);
              $this->db->set('updated_at', date('Y-m-d h:m:s'));
              $this->db->update('ci_aboutus');
          } else {
              $data = array('terms_conditions' => $terms_conditions,
                            'created_at' => date('Y-m-d h:m:s'));
              $this->db->insert('ci_aboutus', $data);
              $this->db->insert_id();
          }         
          
      }
      
      function getAboutUs() {
          
          return $this->db->get('ci_aboutus')->row_array();
      }
      
      function checkActive($user_id) {
          
          /*
          $is_active = $this->db->where('facebook_id', $user_id)->get('ci_users')->row()->is_active;
          
          if ($is_active == 2) {
              return true;
          } else {
              return false;
          }
          */
          $query =  $this->db->get_where('ci_users', array('facebook_id' => $user_id));
          
          return $query->row_array();
      }
      
      function confirmItem($user_id, $item_id) {
          
          $this->db->where('id', $item_id);
          $this->db->set('status', 'Confirmed');
          
          $this->db->update('ci_orders');
          
          
      }
      
  }
?>

<?php 
$cur_tab = $this->uri->segment(1)==''?'dashboard': $this->uri->segment(1);  
?>  

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= ucwords($this->session->userdata('name')); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      
      <ul class="sidebar-menu">
        <!--<li class="header"></li> -->
        
        <li id="users" class="treeview">
            <a href="#">
              <i class="fa fa-users"></i> <span>Users</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?= base_url('admin/users'); ?>"><i class="fa fa-circle-o"></i> View Users</a></li>              
              <!--<li ><a href="<?= base_url('admin/users/add'); ?>"><i class="fa fa-circle-o"></i> Add User</a></li>-->
              
            </ul>
        </li>
        <li id="trransactions" class="treeview">
            <a href="#">
              <i class="fa fa-road"></i> <span>Transactions</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="view_users" class=""><a href="<?= base_url('admin/transactions'); ?>"><i class="fa fa-circle-o"></i> View Transactions</a></li>              
            </ul>
        </li>
        
        <li id="setting" class="treeview">
            <a href="#">
              <i class="fa fa-cog"></i> <span>Setting</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="view_users" class=""><a href="<?= base_url('admin/settings'); ?>"><i class="fa fa-money"></i> Price setting</a></li>              
            </ul>
        </li>

      </ul>
      


    </section>
    <!-- /.sidebar -->
  </aside>

  
<script>
  $("#<?= $cur_tab; ?>").addClass('active');
</script>

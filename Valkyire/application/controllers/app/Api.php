<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';
require_once 'vendor/twilio/sdk/Twilio/autoload.php'; // Loads the library
use Twilio\Rest\Client  as TwilioClient;

class Api extends CI_Controller {

    

    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('app/api_model', 'api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function test(){
        echo "API test is OK:)";
    }
    
    function version() {
        phpinfo();
    }
    
    function vrifyCodeTest() {  
        
        $phone = $_POST['phone_number'];
        $code = $this->makeRandomCode();
        
        $this->sendCode($phone, $code);
    }
    
    function sendCode($phonenumber, $message) {
         
         $result = array();
         
         //$phonenumber = $this->doUrlDecode($phonenumber); 
         
         $result['phone_number'] = $phonenumber;         
         
         // send code via twilio
         $sid = "AC0566dc52512d0994e0bc65be227ccb79";
         $token = "0bd1a3707cd7a52b5c3431f700bb1745";
         
         //sandbox
         //$sid = "AC0bf6beb494852e739fc4529a1723db14";
         //$token = "6a817098224418ba427cc33a007ad226";
         
         $client = new TwilioClient($sid, $token);
        
         try {
            
            $result['result'] = $client->messages->create($phonenumber,
            array(
                'from' => '+17636007055',
                //'from' => 'Valkyrie',
                'body' => $message                
                )
            );
            
         } catch(Exception $e) {
            
             $this->doRespondSuccess($result); 
             return;             
         }
         
         $this->doRespondSuccess($result); 
    }
    
    function sendMessage($phonenumber, $message) {
         
         $result = array();
         
         //$phonenumber = $this->doUrlDecode($phonenumber); 
         
         $result['phone_number'] = $phonenumber;         
         
         // send code via twilio
         $sid = "AC0566dc52512d0994e0bc65be227ccb79";
         $token = "0bd1a3707cd7a52b5c3431f700bb1745";
         
         //sandbox
         //$sid = "AC0bf6beb494852e739fc4529a1723db14";
         //$token = "6a817098224418ba427cc33a007ad226";
         
         $client = new TwilioClient($sid, $token);
        
         try {
            
            $result['result'] = $client->messages->create($phonenumber,
            array(
                'from' => '+17636007055',
                //'from' => 'Valkyrie',
                'body' => $message,
                
            )
            );
            
         } catch(Exception $e) {
            
             $this->doRespondSuccess($result); 
             return;             
         }
         
         $this->doRespondSuccess($result); 
    }
     
    function getPhonenumber($user_id) {
        
        $user = $this->db->where('id', $user_id)->get('ci_users')->row();
        return $user->phonenumber;
    }
    
    public function makeRandomCode(){
         
         mt_srand();

         $random_code = '';

         $arr = array('1','2','3','4','5','6','7','8','9','0');

         for ($i = 0 ; $i < 6 ; $i++) {

             $index = mt_rand(0, 9);
             $random_code .= $arr[$index];
         }

         return $random_code;
     }
    
    /**
    * Send Firebase push notification
    * 
    * @param mixed $user_id
    * @param mixed $type
    * @param mixed $body
    * @param mixed $content
    */
    function sendPush($user_id, $type, $body, $content) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAAPSde5aY:APA91bHgm1XpjkX3HoxLToc4KgP7qEKu4cRf2f40hVnOKM02bb_-s0yfiWTMz1K9_zJNNj5VeVQ06QmsONNruudhIeokdIyMHsSpxZYHBz-cslstDXLT6dh3ETQNQu8uEHtgCg5oJM6o";
        
        $token = "";
        $token = $this->api_model->getToken($user_id);
        
        if (strlen($token) == 0 ) {

            return;
        }
        
        /*
        if(is_array($target)){
            $fields['registration_ids'] = $target;
        } else{
            $fields['to'] = $target;
        }

        // Tpoic parameter usage
        $fields = array
                    (
                        'to'  => '/topics/alerts',
                        'notification'          => $msg
                    );
        $data = array('msgType' => $type,
                      'content' => $content);
        */
        $msg = array
                (
                    'body'     => $body,
                    'title'    => 'Valkyire',   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
        $fields = array
            (
                //'registration_ids'    => $tokens,
                'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        //@curl_exec($ch);
        
        $result['result'] = curl_exec($ch); 

        curl_close($ch); 
        
        return $result;
        
    }
    
    
    /**
    * get user object from query array result
    * 
    * @param mixed $user_array query result array
    */
    function getUser($user_array) {
        
        $user_object = array('user_id' => $user_array['id'],
                             'email' => $user_array['email'],
                             'user_name' => $user_array['username'],
                             'address' => $user_array['address'],
                             'photo_url' => $user_array['photo_url'],
                             'is_active' => $user_array['is_active'] == 1 ? "No" : "Yes",
                             'phone_number' => $user_array['mobile_no']
                             );
        return $user_object;
    }
    
    function roleUser($user_array) {
        
        $user_object = array('user_id' => $user_array['id'],
                             'email' => $user_array['email'],
                             'user_name' => $user_array['username'],
                             'address' => $user_array['address'],
                             'latitude' => $user_array['latitude'],
                             'longitude' => $user_array['longitude'],
                             'phone_number' => $user_array['mobile_no'],
                             'photo_url' => $user_array['photo_url'],
                             'is_active' => $user_array['is_active'] == 1 ? "No" : "Yes",
                             'role' => $user_array['role'] == 2 ? 'driver' : 'customer'
                             );
        return $user_object;
    }

    function signup() {
        
        $result = array();
        
        $user_name = $_POST['user_name'];
        $email = $_POST['email'];
        $phone_number = $_POST['phone_number'];
        $password = $_POST['password'];
        $address = $_POST['address'];
        
        if ($this->api_model->exist_user_email($email) == TRUE) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);
                          
        } else {
            $code = $this->makeRandomCode();
            $msg = "Valkyrie laundry Verification code is : ".$code;
            $this->sendCode($phone_number, $msg);
            $data = array('username' => $user_name,
                          'email' => $email,
                          'mobile_no' => $phone_number,
                          'address' => $address,
                          'verification_code' => $code,
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s'),
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $result["user_id"] = $this->api_model->add_user($data);            
            $this->doRespondSuccess($result);  
        }
    }
    
    function signin() {
        
        $result = array();
        
        $email = $_POST['email'];
        $password = $_POST['password'];
            
        $data = array(
            'email' => $email,
            'password' => $password
        );
        $q_result = $this->api_model->login($data);
        if ($q_result == TRUE) {
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespondSuccess($result);            
        } else{
            $result['message'] = 'Invalid Email or Password!';
            $this->doRespond(202, $result);
        }        
    }
    
    function facebookSignin() {
        
        $result = array();
        
        $user_name = $_POST['user_name'];
        $email = $_POST['email'];
        $facebook_id = $_POST['facebook_id'];
        $phone_number = $_POST['phone_number'];
        $password = $_POST['password'];
        $address = $_POST['address'];
        
        $q_result = $this->api_model->facebookSignin($facebook_id);
        
        if ($q_result == TRUE) {
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespond(203, $result);            
        } else {
            $code = $this->makeRandomCode();
            $msg = "Valkyrie laundry Verification code is : ".$code;
            $this->sendCode($phone_number, $msg);
            
            $data = array('username' => $user_name,
                          'email' => $email,
                          'mobile_no' => $phone_number,
                          'facebook_id' => $facebook_id,
                          'verification_code' => $code,
                          'address' => $address,
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s')
                          );
            $result["user_id"] = $this->api_model->add_user($data);            
            $this->doRespondSuccess($result);  
        }       
        
    }
    
    function validCustomer() {
        
        $result = array();
        $email = $_POST['email'];
        $phone_number = $_POST['phone_number'];
        
        if ($this->api_model->validCustomer($email, $phone_number) == TRUE) {
            
            $code =  $this->makeRandomCode();
            $msg = "Valkyrie laundry Verification code is : ".$code;
            $this->sendCode($phone_number, $msg);
            
            $this->api_model->updateCode($email, $phone_number, $code);
            
            $this->doRespondSuccess($result);
            
        } else {
            $result['message'] = "Invalid customer";
            $this->doRespond(208, $result);
        }
    }
    
    function forgotPassword() {
        
        $result = array();
        
        $email = $_POST['email'];        
        $verification_code = $_POST['verification_code'];
        $password = $_POST['password'];
        
        if ($this->api_model->validVerifyCode($email, $verification_code)) {
          
            $data = array('email' => $email,                          
                          'password' => password_hash($password, PASSWORD_BCRYPT)                           
                          );
            $this->api_model->resetPassword($data); 
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Invalid Email and Code.";
            $this->doRespond(204, $result);
        }
    }
    
    function getServiceModel($service_array) {
        
        $service_model = array('service_id' => $service_array['id'],
                               'service_type' => $service_array['service_type'],
                               'category' => $service_array['category'],
                               'service_name' => $service_array['service_name'],
                               'price' => $service_array['price'],
                               'photo_url' => $service_array['photo_url']
                              );
        return $service_model;
    }
    
    function getServiceList() {
        
        $result = array();
        $service_array = array();
        
        $service_type = $_POST['service_type'];
        
        $q_result = $this->api_model->getServiceList($service_type);
        
        if ($q_result == false) {
            $result['message'] = "No data.";
            $this->doRespond(205, $result);
        } else {
            
            foreach ($q_result as $service_model) {                  
                $service = $this->getServiceModel($service_model);                
                array_push($service_array, $service);                
            }
            
            $result['service_list'] = $service_array;
            $this->doRespondSuccess($result);            
        } 
    }
    
    function getOrderModel($order_array) {
        
        
        $order_model = array('order_id' => $order_array['id'],
                               'customer_id' => $order_array['user_id'],                                 
                               'customer_name' => $order_array['user_name'],                                 
                               'phone_number' => $order_array['mobile_no'],                                 
                               'invoice_number' => $order_array['invoice_number'],                                 
                               'total_itmes' => $order_array['total_items'],
                               'total_cost' => $order_array['total_cost'],
                               'latitude' => $order_array['latitude'],
                               'longitude' => $order_array['longitude'],
                               'address' => $order_array['address'],
                               'invoice_date' => $order_array['ordered_at'],
                               'ordered_at' => $order_array['created_at'],
                               'status' => $order_array['status']
                              );
        return $order_model;
        
        
    }
    
    function makeOrder() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $user_name = $_POST['user_name'];
        $invoice_number = $_POST['invoice_number'];
        $invoice_date = $_POST['invoice_date'];
        $address = $_POST['address'];
        $total_items = $_POST['total_items'];
        $total_cost = $_POST['total_cost'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        
        $data = array('user_id' => $user_id,
                      'user_name' => $user_name,
                      'invoice_number' => $invoice_number,
                      'address' => $address,
                      'total_items' => $total_items,
                      'total_cost' => $total_cost,
                      'latitude' => $latitude,
                      'longitude' => $longitude,
                      'ordered_at' => $invoice_date,
                      'created_at' => date('Y-m-d h:m:s'),
                      'status' => 'Ordered'
                      );
        
        $order_id = $this->api_model->makeOrder($data); 
        $result['order_id'] = $order_id;    
        $this->doRespondSuccess($result);        
        
    }
    
    function getMyOrders() {
        
        $result = array();
        $order_array = array();
        
        $user_id = $_POST['user_id'];
        
        $q_result = $this->api_model->getMyOrders($user_id);
        
        if ($q_result == false) {
            
            $result['message'] = "No data";
            $this->doRespond(205, $result);
        } else {
            foreach ($q_result as $order) {
                
                $order_model = $this->getOrderModel($order);
                array_push($order_array, $order_model);
            }
            
            $result['order_list'] =  $order_array;
            $this->doRespondSuccess($result);
        }
        
    }
    
    // Driver and manager app
    
    function addServiceItem() {
        
        $result = array();

                  
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $_POST['service_name'].'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            
            $data = array('service_type' => $_POST['service_type'],
                          'category' => $_POST['category'],
                          'service_name' => $_POST['service_name'],
                          'price' => $_POST['price'],
                          'photo_url' => $file_url,
                          'created_at' => date('Y-m-d h:m:s'),
                          'updated_at' => date('Y-m-d h:m:s')
                      );             
            $id = $this->api_model->addServiceItem($data);
            $result['upload_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }
    
        
    function stuffSignin() {
        
        $result = array();
        
        $email = $_POST['email'];
        $password = $_POST['password'];
            
        $data = array(
            'email' => $email,
            'password' => $password
        );
        
        $q_result = $this->api_model->stuffLogin($data);
        
        if ($q_result == TRUE) {
            $result['user_model'] = $this->roleUser($q_result);
            $this->doRespondSuccess($result);            
        } else{
            $result['message'] = 'Invalid Email or Password!';
            $this->doRespond(202, $result);
        }        
    }
    
    function stuffForgotPassword() {
        
        $result = array();
        
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        if ($this->api_model->exist_stuff_user_email($email)) {
            $data = array('email' => $email,                          
                          'password' => password_hash($password, PASSWORD_BCRYPT)                           
                          );
            $this->api_model->stuffResetPassword($data); 
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Invalid Email.";
            $this->doRespond(204, $result);
        }
    }
    
    function addDriver() {
        
        $result = array();
        
        $user_name = $_POST['user_name'];
        $email = $_POST['email'];
        $phone_number = $_POST['phone_number'];
        $password = $_POST['password'];
        
        
        if ($this->api_model->exist_user_email($email)) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);             
        } else {
            $data = array('username' => $user_name,
                          'email' => $email,
                          'mobile_no' => $phone_number,                          
                          'role' => 2,                          
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s'),
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $result["user_id"] = $this->api_model->add_user($data);            
            $this->doRespondSuccess($result);  
        }
    }
    
    function updateDriverLocation() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $address = $_POST['address'];
    
        $data = array('latitude' => $latitude,
                      'longitude' => $longitude,
                      'address' => $address,
                      'updated_at' => date('Y-m-d h:m:s')
                      );
        $this->api_model->update_driver($user_id, $data);            
        $this->doRespondSuccess($result);         
    }
    
    function getDriverCustomerList() {
        
        $result = array();
        $user_array = array();
        
        $q_result = $this->api_model->getDriverCustomerList();
        
        if ($q_result == false) {
            $result['message'] = "No data.";
            $this->doRespond(205, $result);
        } else {
            
            foreach ($q_result as $user_model) {                  
                $user = $this->roleUser($user_model);                
                array_push($user_array, $user);                
            }
            
            $result['user_list'] = $user_array;
            $this->doRespondSuccess($result);            
        }
        
    }
    
    function getCustomerList() {
        
        $result = array();
        $user_array = array();
        
        $q_result = $this->api_model->getCustomerList();
        
        if ($q_result == false) {
            $result['message'] = "No data.";
            $this->doRespond(205, $result);
        } else {
            
            foreach ($q_result as $user_model) {                  
                $user = $this->roleUser($user_model);                
                array_push($user_array, $user);                
            }
            
            $result['user_list'] = $user_array;
            $this->doRespondSuccess($result);            
        }        
    }
    
    function getOrderList() {
        
        $result = array();
        $order_array = array();         
        
        $q_result = $this->api_model->getOrderList();
        
        if ($q_result == false) {
            
            $result['message'] = "No data";
            $this->doRespond(205, $result);
        } else {
            foreach ($q_result as $order) {
                
                $order_model = $this->getOrderModel($order);
                array_push($order_array, $order_model);
            }
            
            $result['order_list'] =  $order_array;
            $this->doRespondSuccess($result);
        }
        
    }
    
    function sendOffCode() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $off_code = $_POST['off_code'];
        $discount = $_POST['discount'];
        $message = $_POST['about_offer'];         
        
        $data = array ('user_id' => $user_id,
                        'off_code' => $off_code,
                        'discount' => $discount,
                        'about_offer' => $message,
                        'created_at' => date('Y-m-d h:m:S')
                        );
                        
        $this->api_model->sendOffCode($user_id, $data);
        
        $phone_number = $this->db->get_where('ci_users', array('id' => $user_id))->row()->mobile_no;
        $message = "Valkyrie team is interested in providing your own discount code , because you are a special customer\n
Your code is : " . $off_code;
        $this->sendMessage($phone_number, $message);
        
        $this->doRespondSuccess($result);        
        
    }
    
    function getMyOffCode() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        
        $q_result =  $this->api_model->getMyOffCode($user_id);
        
        if ($q_result == FALSE) {            
            $result['message'] = "You don't have off code";
            $this->doRespond(206, $result);
        } else {
             $result = $q_result;
             $this->doRespondSuccess($result);
        }
    }
    
    function uploadPhoto() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $user_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            
            $data = array('photo_url' => $file_url,
                          'updated_at' => date('Y-m-d h:m:s')
                      );             
            $id = $this->api_model->updatePhoto($user_id, $data);
            $result['upload_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }
    
    function updateServiceItem() {         

        $result = array();
        
        $service_id = $_POST['service_id'];
        
        $data = array('service_type' => $_POST['service_type'],
                      'category' => $_POST['category'],                       
                      'service_name' => $_POST['service_name'],
                      'price' => $_POST['price'], 
                      'updated_at' => date('Y-m-d h:m:s')
                  );             
        $id = $this->api_model->updateServiceItem($service_id, $data);
                  
        $this->doRespondSuccess($result);

        
    }
    
    function updateServicePhoto() {
        
        $result = array();
        
        $service_id = $_POST['service_id'];
        
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $service_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            
            $data = array('photo_url' => $file_url,
                          'updated_at' => date('Y-m-d h:m:s')
                      );             
            $id = $this->api_model->updateServicePhoto($service_id, $data);
            $result['upload_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }
    
    function removeDriver() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        
        $this->api_model->removeDriver($user_id);
        
        $this->doRespondSuccess($result);
    }
    
    function verifyPhoneNumber() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $code = $_POST['verification_code'];
        
        if ($this->api_model->verifyPhoneNumber($user_id, $code) == TRUE) {
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Invalide code";
            $this->doRespond(207, $result);
        }        
    }
    
    function updateMyAccount() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $user_name = $_POST['user_name'];
        $email = $_POST['email'];
        $phone_number = $_POST['phone_number'];
        $password = $_POST['password'];
        $address = $_POST['address'];
        
        if ($this->api_model->exist_user_email_id($user_id, $email) == TRUE) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);
                          
        } else {
            
            $data = array('username' => $user_name,
                          'email' => $email,
                          'mobile_no' => $phone_number,
                          'address' => $address,                           
                          'password' => password_hash($password, PASSWORD_BCRYPT),                           
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $this->api_model->update_user($user_id, $data);            
            $this->doRespondSuccess($result);  
        }
    }
    
    function customerDetailOrder() {
        
        
    }
    
    function setPrivacyPolicy() {
        
        $result = array();
        $privacy_policy = $_POST['privacy_policy'];
        
        $this->api_model->setPrivacyPolicy($privacy_policy);
        
        $this->doRespondSuccess($result);
    }
    
    function setDistinguishUs() {
        
        $result = array();
        $distinguish_us = $_POST['distinguish_us'];
        
        $this->api_model->setDistinguishUs($distinguish_us);
        
        $this->doRespondSuccess($result);
    }
    
    function setTermsConditions() {
        
        $result = array();
        $terms_conditions = $_POST['terms_conditions'];
        
        $this->api_model->setTermsConditions($terms_conditions);
        
        $this->doRespondSuccess($result);
    }
    
    function getAboutUs() {
        
        $result = array();
        
        $result = $this->api_model->getAboutUs();
        
        $this->doRespondSuccess($result);
    }
    
    function checkActive() {
        
         $result = array();
         $q_result = array();
         $user_id = $_POST['facebook_id'];
         
         /*
         if ($this->api_model->checkActive($user_id) == true) {
             
             $this->doRespondSuccess($result); 
         } else {
             $result['message'] = "This user is not activated";
             $this->doRespond(211, $result);
         } */
         
         $q_result = $this->api_model->checkActive($user_id);
         //print_r($q_result);
         $result['user_model'] = $this->getUser($q_result);
         $this->doRespondSuccess($result); 
    }
    

    
    function confirmItem() {
        
         $result = array();
         
         $item_id = $_POST['item_id'];          
         $user_id = $_POST['user_id'];          
        
         $this->api_model->confirmItem($user_id, $item_id);
             
         $this->doRespondSuccess($result); 
         
        
    }
    
}
?>

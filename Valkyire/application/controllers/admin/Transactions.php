<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Transactions extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('admin/transaction_model', 'transaction_model');
            $this->load->helper('url');
            $this->load->library('session');
        }

        public function index(){
            $data['all_transactions'] =  $this->transaction_model->get_all_transactions();
            $data['title'] = 'Transaction List';
            $data['view'] = 'admin/transactions/transaction_list';
            $this->load->view('admin/layout', $data);
        }       
    
        //-----------------------------------------------------------------\
        // File Return Portal
        public function file_return($id = 0){
            
            if($this->input->post('submit')){

                $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
                $this->form_validation->set_rules('transaction_id', 'Transaction ID', 'trim|required');
                $this->form_validation->set_rules('editor_id', 'Editor ID', 'trim|required');
                $this->form_validation->set_rules('comment', 'Comment', 'trim|required');                
                if (empty($_FILES['file']['name']))
                {
                    $this->form_validation->set_rules('file', 'File', 'required');
                }
                
                if ($this->form_validation->run() == FALSE) {
                    $data['title'] = 'Return File';                     
                    $data['view'] = 'admin/transactions/file_return';
                    $data['photo_id'] = $id;
                    $this->load->view('admin/layout', $data);
                }
                else {
                    
                    if(isset($_FILES['file'])){
                        $errors= array();
                        
                        $file_name = $_FILES['file']['name'];
                        $file_size =$_FILES['file']['size'];
                        $file_tmp =$_FILES['file']['tmp_name'];
                        $file_type=$_FILES['file']['type'];
                        //$file_ext=strtolower(end(explode('.',$_FILES['file']['name'])));
                        
                        $file_ext = pathinfo($file_name, PATHINFO_EXTENSION);

                        $expensions= array("jpeg","jpg","png");                        
                        
                        if(in_array($file_ext,$expensions)=== false){
                            $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                        }

                        if($file_size > 5 * 1024 * 1024){
                            $errors[]='File size must be excatly 5 MB';
                        }
                        if(empty($errors)==true){
                            
                            if(!is_dir("uploadfiles/")) {
                                mkdir("uploadfiles/");
                            }
                            $upload_path = "uploadfiles/";  

                            $cur_time = time();

                            $dateY = date("Y", $cur_time);
                            $dateM = date("m", $cur_time);

                            if(!is_dir($upload_path."/".$dateY)){
                                mkdir($upload_path."/".$dateY);
                            }
                            if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                                mkdir($upload_path."/".$dateY."/".$dateM);
                            }

                            $upload_path .= $dateY."/".$dateM."/";
                            $upload_url = base_url().$upload_path;
                            
                            // Upload file. 

                            $w_uploadConfig = array(
                                'upload_path' => $upload_path,
                                'upload_url' => $upload_url,
                                'allowed_types' => "jpeg|jpg|png",
                                'overwrite' => TRUE,
                                'max_size' => "100000KB",
                                'max_width' => 3000,
                                'max_height' => 4000,
                                'file_name' => 'Finish_'.$id.'_'.date('Y-m-d')
                            );

                            $this->load->library('upload', $w_uploadConfig);

                            if ($this->upload->do_upload('file')) {

                                $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                                
                                /*
                                $data = array(
                                    'transaction_id' => $this->input->post('transaction_id'),
                                    'editor_id' => $this->input->post('editor_id'),
                                    'comment' => $this->input->post('comment'),
                                    'origin_id' => $id,
                                    'email' => $this->input->post('email'),                        
                                    'file_url' => $file_url,                        
                                    'finished_at' => date('Y-m-d h:m:s'),
                                );
                                $data = $this->security->xss_clean($data);
                                $result = $this->transaction_model->finish_return($data);
                                */
                                // Mail Function

                                require_once "Mail.php";
                                $smtpmailto = $this->input->post('email');
                                $smtpmailfrom = "Kaali <beautistar327@gmail.com>";
                                $smtpsubject = "Kaali File Return";
                                $smtphost = "ssl://smtp.gmail.com";
                                $smtpport = "465";
                                $smtpusername = "beautistar327@gmail.com";
                                $smtppassword = "wgpgdihtngzqytmc";

                                $content = file_get_contents($file_url);
                                $content = chunk_split(base64_encode($content));
                                $uid = md5(uniqid(time()));

                                $mime = "1.0";
                                $content_type = "multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
                                // header
                                $headers = array ('From' => $smtpmailfrom,
                                  'To' => $smtpmailto,
                                  'Subject' => $smtpsubject,
                                  'Reply-To' => $this->input->post('email'),
                                  'MIME-Version' => $mime,
                                  'Content-Type' => $content_type);


                                /*
                                $header = "From: Kaali <beautistar327@gmail.com>\r\n";
                                $header .= "Reply-To: <".$this->input->post('email').">\r\n";
                                $header .= "Subject: <".$smtpsubject.">\r\n";
                                $header .= "MIME-Version: 1.0\r\n";
                                $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
                                */

                                $message = "Transaction ID: ".$this->input->post('transaction_id')."\r\n";
                                $message .= "Editor ID: ".$this->input->post('editor_id')."\r\n";
                                $message .= "Comment: ".$this->input->post('comment')."\r\n";
                                $message .= "Email: ".$this->input->post('email')."\r\n";

                                // message & attachment
                                $nmessage = "--".$uid."\r\n";
                                $nmessage .= "Content-type:text/plain; charset=iso-8859-1\r\n";
                                $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
                                $nmessage .= $message."\r\n\r\n";
                                $nmessage .= "--".$uid."\r\n";
                                $nmessage .= "Content-Type: application/octet-stream; name=\"".$this->upload->file_name."\"\r\n";
                                $nmessage .= "Content-Transfer-Encoding: base64\r\n";
                                $nmessage .= "Content-Disposition: attachment; filename=\"".$this->upload->file_name."\"\r\n\r\n";
                                $nmessage .= $content."\r\n\r\n";
                                $nmessage .= "--".$uid."--";

                                $smtp = Mail::factory('smtp',
                                  array ('host' => $smtphost,
                                    'port' => $smtpport,
                                    'auth' => true,
                                    'username' => $smtpusername,
                                    'password' => $smtppassword));
                                $mail = $smtp->send($smtpmailto, $headers, $nmessage);

                                // End Mail Function

                                $data = array(
                                    //'transaction_id' => $this->input->post('transaction_id'),
                                    'editor_id' => $this->input->post('editor_id'),
                                    'editor_comment' => $this->input->post('comment'),
                                    //'origin_id' => $id,
                                    //'email' => $this->input->post('email'),                        
                                    'finished_file' => $file_url,                        
                                    'returned_at' => date('Y-m-d h:m:s'),
                                    'status' => 1,
                                );
                                $data = $this->security->xss_clean($data);
                                
                                $result = $this->transaction_model->update_order($id, $data);
                                if($result){
                                    $this->session->set_flashdata('msg', 'File has been returned Successfully!');
                                    redirect(base_url('admin/transactions'));                     
                                }
                            } else {

                                $this->session->set_flashdata('msg', "Returning file has been failed, Try again!");
                                $data['title'] = 'Return File';
                                $data['view'] = 'admin/transactions/file_return';
                                $data['photo_id'] = $id;
                                $this->load->view('admin/layout', $data);
                            }
                            
                        }else{
                         
                            $this->session->set_flashdata('msg', $errors);
                            $data['title'] = 'Return File';
                            $data['view'] = 'admin/transactions/file_return';
                            $data['photo_id'] = $id;
                            $this->load->view('admin/layout', $data); 
                        }                        
                    }    
                }
            }
            else {                
                $data['title'] = 'Return File';
                $data['view'] = 'admin/transactions/file_return';
                $data['photo_id'] = $id;
                $this->load->view('admin/layout', $data);
            }
            
        }
        
        //----------------------------------------------------------------
        // Download photo
        
        function download($photo_id, $type){ //assign value from $row->file from data_ebook page
        
            if ($photo_id) {
                $this->load->helper('download');
                
                if ($type == 0) {
                    $file_url = $this->transaction_model->get_photourl_by_id($photo_id);                    
                    $name='Raw_photo_'.$photo_id.'_'.date('Y-m-d').'.jpg';                     
                } else {
                    $file_url = $this->transaction_model->get_finished_photourl_by_id($photo_id);
                    $name='Finished_photo_'.$photo_id."_".date('Y-m-d').'.jpg';                    
                }
                $data=file_get_contents($file_url);                
                if (force_download($name, $data)) {
                    redirect("admin/translations");
                }                
            }
        }
    }


?>
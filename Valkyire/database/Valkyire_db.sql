/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.7.22-0ubuntu0.16.04.1 : Database - valkyire
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`valkyire` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `valkyire`;

/*Table structure for table `ci_offcode` */

DROP TABLE IF EXISTS `ci_offcode`;

CREATE TABLE `ci_offcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `off_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `about_offer` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_offcode` */

insert  into `ci_offcode`(`id`,`user_id`,`off_code`,`about_offer`,`created_at`) values (1,2,'123456','This is offcode','2018-05-19 03:05:00');

/*Table structure for table `ci_orders` */

DROP TABLE IF EXISTS `ci_orders`;

CREATE TABLE `ci_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_items` int(11) NOT NULL,
  `total_cost` float(10,2) NOT NULL,
  `ordered_at` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float(15,8) NOT NULL,
  `longitude` float(15,8) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_orders` */

insert  into `ci_orders`(`id`,`user_id`,`user_name`,`invoice_number`,`address`,`total_items`,`total_cost`,`ordered_at`,`latitude`,`longitude`,`status`) values (1,2,'','','',3,10.00,'2018-05-03 03:05:43',0.00000000,0.00000000,'Ordered'),(2,2,'','','',3,10.00,'2018-05-03 03:05:02',0.00000000,0.00000000,'Ordered'),(3,2,'','','',3,10.00,'2018-05-03 06:05:46',0.00000000,0.00000000,'Ordered'),(4,2,'','','',3,10.00,'2018-05-07 11:05:30',0.00000000,0.00000000,'Ordered'),(5,2,'','','',3,10.00,'2018-05-08 10:05:50',0.00000000,0.00000000,'Ordered'),(6,2,'','','',3,10.00,'2018-05-09 04:05:34',0.00000000,0.00000000,'Ordered'),(7,2,'','','',3,10.00,'2018-05-10 11:05:36',0.00000000,0.00000000,'Ordered'),(8,2,'','','',3,10.00,'2018-05-11 12:05:56',0.00000000,0.00000000,'Ordered'),(9,2,'','','',3,10.00,'2018-05-14 04:05:39',0.00000000,0.00000000,'Ordered'),(10,2,'','','',3,10.00,'2018-05-14 04:05:36',0.00000000,0.00000000,'Ordered'),(11,3,'','','',3,10.00,'2018-05-14 04:05:00',0.00000000,0.00000000,'Ordered'),(12,3,'','','',3,3.00,'2018-05-14 06:05:19',0.00000000,0.00000000,'Ordered'),(13,3,'','','',3,3.00,'2018-05-14 06:05:48',0.00000000,0.00000000,'Ordered'),(14,3,'','','',3,3.00,'2018-05-14 06:05:30',0.00000000,0.00000000,'Ordered'),(15,3,'','','',3,3.00,'2018-05-14 07:05:40',0.00000000,0.00000000,'Ordered'),(16,3,'','','',3,3.00,'2018-05-14 07:05:47',0.00000000,0.00000000,'Ordered'),(17,101,'','','',3,10.00,'2018-05-14 07:05:30',0.00000000,0.00000000,'Ordered'),(18,0,'','','',3,10.00,'2018-05-14 07:05:37',0.00000000,0.00000000,'Ordered'),(19,3,'','','',3,3.00,'2018-05-14 07:05:27',0.00000000,0.00000000,'Ordered'),(20,0,'','','',3,3.00,'2018-05-14 08:05:27',0.00000000,0.00000000,'Ordered'),(21,0,'','','',3,3.00,'2018-05-14 08:05:29',0.00000000,0.00000000,'Ordered'),(22,0,'','','',3,3.00,'2018-05-14 08:05:31',0.00000000,0.00000000,'Ordered'),(23,0,'','','',3,3.00,'2018-05-14 09:05:45',0.00000000,0.00000000,'Ordered'),(24,0,'','','',3,3.00,'2018-05-14 09:05:24',0.00000000,0.00000000,'Ordered'),(25,0,'','','',3,3.00,'2018-05-14 09:05:34',0.00000000,0.00000000,'Ordered'),(26,0,'','','',3,3.00,'2018-05-14 09:05:43',0.00000000,0.00000000,'Ordered'),(27,0,'','','',3,3.00,'2018-05-14 09:05:29',0.00000000,0.00000000,'Ordered'),(28,0,'','','',3,3.00,'2018-05-14 09:05:10',0.00000000,0.00000000,'Ordered'),(29,0,'','','',3,3.00,'2018-05-14 09:05:48',0.00000000,0.00000000,'Ordered'),(30,3,'','','',3,3.00,'2018-05-14 09:05:36',0.00000000,0.00000000,'Ordered'),(31,0,'','','',3,3.00,'2018-05-14 11:05:48',0.00000000,0.00000000,'Ordered'),(32,0,'','','',3,3.00,'2018-05-14 11:05:31',0.00000000,0.00000000,'Ordered'),(33,3,'','','',3,3.00,'2018-05-14 11:05:17',0.00000000,0.00000000,'Ordered'),(34,0,'','','',3,3.00,'2018-05-14 12:05:13',0.00000000,0.00000000,'Ordered'),(35,0,'','','',3,3.00,'2018-05-14 12:05:30',0.00000000,0.00000000,'Ordered'),(36,3,'','','',3,2.00,'2018-05-14 08:05:47',0.00000000,0.00000000,'Ordered'),(37,3,'','','',3,3.00,'2018-05-15 04:05:53',0.00000000,0.00000000,'Ordered'),(38,3,'','','',9,9.00,'2018-05-15 05:05:28',0.00000000,0.00000000,'Ordered'),(39,3,'','','',22,15.00,'2018-05-15 07:05:26',0.00000000,0.00000000,'Ordered'),(40,0,'','','',3,12.00,'2018-05-15 09:05:23',0.00000000,0.00000000,'Ordered'),(41,3,'','','',3,25.00,'2018-05-17 11:05:47',0.00000000,0.00000000,'Ordered'),(42,3,'','','',2,5.00,'2018-05-17 11:05:29',0.00000000,0.00000000,'Ordered'),(43,3,'','','',4,49.00,'2018-05-17 11:05:39',0.00000000,0.00000000,'Ordered'),(44,3,'','','',3,10.00,'2018-05-17 12:05:34',0.00000000,0.00000000,'Ordered'),(45,3,'','','',4,26.00,'2018-05-17 12:05:33',0.00000000,0.00000000,'Ordered'),(46,3,'','','',2,25.00,'2018-05-17 12:05:34',0.00000000,0.00000000,'Ordered'),(47,3,'','','',4,12.00,'2018-05-17 12:05:37',0.00000000,0.00000000,'Ordered'),(48,15,'','','',3,41.00,'2018-05-17 01:05:16',0.00000000,0.00000000,'Ordered'),(49,15,'','','',3,41.00,'2018-05-17 01:05:51',0.00000000,0.00000000,'Ordered'),(50,15,'','','',1,1.00,'2018-05-17 01:05:17',0.00000000,0.00000000,'Ordered'),(51,15,'','','',0,0.00,'2018-05-17 01:05:06',0.00000000,0.00000000,'Ordered'),(52,15,'','','',0,0.00,'2018-05-17 01:05:07',0.00000000,0.00000000,'Ordered'),(53,2,'test','11111','yanji small town 69 load',3,10.00,'2018/05/17',129.65846252,39.15420151,'Ordered');

/*Table structure for table `ci_services` */

DROP TABLE IF EXISTS `ci_services`;

CREATE TABLE `ci_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `service_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` float(10,2) NOT NULL,
  `photo_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_services` */

insert  into `ci_services`(`id`,`service_type`,`category`,`service_name`,`price`,`photo_url`,`created_at`,`updated_at`) values (1,'Clothes','Men','T-shirt',1.00,'http://52.66.111.241/uploadfiles/2018/05/T-shirt_15253178113.jpg','2018-05-03 03:05:31','2018-05-03 03:05:31'),(2,'Clothes','Women','Clothes',2.00,'http://52.66.111.241/uploadfiles/2018/05/T-shirt_15263067849.jpg','2018-05-14 02:05:24','2018-05-14 02:05:24'),(3,'Clothes','Women','clothes',5.00,'http://52.66.111.241/uploadfiles/2018/05/T-shirt_15263068940.jpg','2018-05-14 02:05:14','2018-05-14 02:05:14'),(4,'Clothes','Kids','Clothes',4.00,'http://52.66.111.241/uploadfiles/2018/05/T-shirt_15263069547.jpg','2018-05-14 02:05:14','2018-05-14 02:05:14'),(5,'Blankets','','Blanket',4.00,'http://52.66.111.241/uploadfiles/2018/05/T-shirt_15263070470.jpg','2018-05-14 02:05:46','2018-05-14 02:05:46'),(6,'Blankets','','Blanket',8.00,'http://52.66.111.241/uploadfiles/2018/05/T-shirt_15263070577.jpg','2018-05-14 02:05:57','2018-05-14 02:05:57'),(7,'Curtains','','Curtain',15.00,'http://52.66.111.241/uploadfiles/2018/05/T-shirt_15263071612.jpg','2018-05-14 02:05:41','2018-05-14 02:05:41'),(8,'Specials','','Tuxedos',15.00,'http://52.66.111.241/uploadfiles/2018/05/Tuxedos_15263074643.jpg','2018-05-14 02:05:44','2018-05-14 02:05:44'),(9,'Shoes','Men','Tuxedos',15.00,'http://52.66.111.241/uploadfiles/2018/05/Tuxedos_15263074998.jpg','2018-05-14 02:05:19','2018-05-14 02:05:19'),(10,'Shoes','Men','Shoes',15.00,'http://52.66.111.241/uploadfiles/2018/05/Shoes_15263075179.jpg','2018-05-14 02:05:37','2018-05-14 02:05:37'),(11,'Shoes','Men','Socks',15.00,'http://52.66.111.241/uploadfiles/2018/05/Socks_15263075418.jpg','2018-05-14 02:05:01','2018-05-14 02:05:01'),(12,'Underwear','Women','Trousers',8.00,'http://52.66.111.241/uploadfiles/2018/05/Trousers_15263078131.jpg','2018-05-14 02:05:33','2018-05-14 02:05:33'),(13,'Underwear','Women','Trousers',8.00,'http://52.66.111.241/uploadfiles/2018/05/Trousers_15263078271.jpg','2018-05-14 02:05:47','2018-05-14 02:05:47'),(14,'Underwear','Men','Trousers',8.00,'http://52.66.111.241/uploadfiles/2018/05/Trousers_15263079174.jpg','2018-05-14 02:05:17','2018-05-14 02:05:17'),(15,'Underwear','Men','Semi-Trousers',4.00,'http://52.66.111.241/uploadfiles/2018/05/Semi-Trousers_15263079818.jpg','2018-05-14 02:05:21','2018-05-14 02:05:21'),(16,'Clothes','Women','Dress',20.00,'http://52.66.111.241/uploadfiles/2018/05/Semi-Trousers_15263080119.jpg','2018-05-14 02:05:51','2018-05-14 02:05:51'),(17,'Clothes','Women','dress',20.00,'http://52.66.111.241/uploadfiles/2018/05/dress_15263080326.jpg','2018-05-14 02:05:12','2018-05-14 02:05:12');

/*Table structure for table `ci_users` */

DROP TABLE IF EXISTS `ci_users`;

CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook_id` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `latitude` float(15,8) NOT NULL,
  `longitude` float(15,8) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:customer, 2:driver, 3:manager',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `ci_users` */

insert  into `ci_users`(`id`,`facebook_id`,`username`,`firstname`,`lastname`,`email`,`mobile_no`,`password`,`address`,`latitude`,`longitude`,`role`,`is_active`,`is_admin`,`last_ip`,`created_at`,`updated_at`) values (1,'','Admin','admin','admin','admin@admin.com','123456','$2y$10$8T0rYWBgxjO7fA8pS2L2Fe7CMnjx17pxPOHSRIddI1cjZJs6XDFAq','',0.00000000,0.00000000,1,1,1,'','2018-04-22 20:28:36','2018-04-22 20:28:41'),(2,'','test','','','test@user.com','1234567890','$2y$10$w4yCtX9OEfRqdP1HhXkccO.cJ2BriAjlejnrImcBvqm9qT5AcRlqC','test state test city',0.00000000,0.00000000,1,1,0,'','2018-05-03 02:05:27','2018-05-03 02:05:27'),(3,'','test1','','','test1@user.com','1234567890','$2y$10$fHeO.ebI.KWm2HnuarSF7OrH.KmlfLn9HOTiq66TfbHC/wDNXFDy.','test state test city',0.00000000,0.00000000,1,1,0,'','2018-05-03 03:05:03','2018-05-03 03:05:03'),(4,'11111111111','test1','','','test1@user.com','1234567890','$2y$10$fHeO.ebI.KWm2HnuarSF7OrH.KmlfLn9HOTiq66TfbHC/wDNXFDy.','test state test city',0.00000000,0.00000000,1,1,0,'','2018-05-03 03:05:31','0000-00-00 00:00:00'),(5,'11111111112','test1','','','test1@user.com','1234567890','$2y$10$fHeO.ebI.KWm2HnuarSF7OrH.KmlfLn9HOTiq66TfbHC/wDNXFDy.','test state test city',0.00000000,0.00000000,1,1,0,'','2018-05-03 03:05:08','0000-00-00 00:00:00'),(6,'','','','','test1@user1.com','','$2y$10$Dohh8x759lSjnxJXPLW9v.K4U5qLP88p.kTP4wu1spqSGTUvP3NQ2','',0.00000000,0.00000000,1,1,0,'','2018-05-07 11:05:23','2018-05-07 11:05:23'),(7,'','','','','test1@user2.com','','$2y$10$5rsVSeDS5eqhg4fkjOgzM.LtUI5TeAm8AObEJkm9rTYwoOXA5B9tu','',0.00000000,0.00000000,1,1,0,'','2018-05-07 11:05:37','2018-05-07 11:05:37'),(8,'','','','','test1@user3.com','','$2y$10$mOgvhYfqkUSQDlDhQY7B6O.zu./GKi1RNodGErT3hrVVDKa4X2O4u','',0.00000000,0.00000000,1,1,0,'','2018-05-07 12:05:42','2018-05-07 12:05:42'),(9,'chauhannishit88@gmail.com','test1','','','test1@user.com','1234567890','$2y$10$GSYGTHvcB0n1ku5bfECTTOKkxCcCyuQravLmsxSA0M6NNm88cehiG','test state test city',0.00000000,0.00000000,1,1,0,'','2018-05-09 05:05:54','0000-00-00 00:00:00'),(10,'1852216671511419','Nishit Chauhan','','','null','null','$2y$10$xtIyrDdHZ0AqoROgJPlwCOYxdXYfyUbkqVMR2A.OinDp4MNg98WLm','null',0.00000000,0.00000000,1,1,0,'','2018-05-10 11:05:26',''),(11,'','test20','','','test20@user.com','1111111111','$2y$10$xyoY.LC0paUKZbaiRAw2huG6IKhTslL.qQYD6JMQ0vRcjKsBVEuQK','yanji',0.00000000,0.00000000,1,1,0,'','2018-05-11 12:05:02','2018-05-11 12:05:02'),(12,'1028078960677847','Salman Adi','','','null','null','$2y$10$cMhX4SwDakhU4Aen7OimDejUKVvnpnXgu1BVzxbsLCi4VI2NQoK6G','null',0.00000000,0.00000000,1,1,0,'','2018-05-12 04:05:05',''),(13,'1856498064416613','Nishit Chauhan','','','null','null','$2y$10$iHZId84MctVrMAnuM5ItwO5K9IR8QHPCeemwmgFTnLzELLB7NhGZC','null',0.00000000,0.00000000,1,1,0,'','2018-05-14 05:05:42',''),(14,'','Test2','','','Test2@user.com','7896543210','$2y$10$o0P.XsYgarVp9RRYO4E4k.FlzojVP7xkxbjRaVZPSeKg9pcBti/Ym','Testcity',0.00000000,0.00000000,1,1,0,'','2018-05-14 07:05:37','2018-05-14 07:05:37'),(15,'','test517','','','test517@user.com','1234567890','$2y$10$Xc/GoGcrwp1T.fJ2h21SROQTMUJ/aVRRfLpH4HwYu4TwvknEszNfi','yanji',0.00000000,0.00000000,1,1,0,'','2018-05-17 12:05:34','2018-05-17 12:05:34'),(16,'240835079807072','Abo Rami Tonesh','','','null','null','$2y$10$w3xu4OX3NjoVUE7K/egTvueyhaaxVpXD8beXqVBGeZPBLvw0AxwOu','null',0.00000000,0.00000000,1,1,0,'','2018-05-17 10:05:49',''),(17,'','Salman','','','Salman.anas.adi@gmail.com','0789340608','$2y$10$3VFAdrW3CjRd9KCwZJfAXudBxxIHLla4PtRYisFhOnt7BksiS08Oi','Amman',0.00000000,0.00000000,1,1,0,'','2018-05-17 10:05:13','2018-05-17 10:05:13'),(18,'','S','','','Salman.adi@hotmail.com','00962789340608','$2y$10$kNCQWiI7HIzC7NE9BBQhH.66YNUEVH2ifuGj.vsLVE96TWacWdGn.','Amman',0.00000000,0.00000000,1,1,0,'','2018-05-17 10:05:38','2018-05-17 10:05:38');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
